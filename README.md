<h1>Party Parrot App</h1>

<img src='media/images/party-parrot.gif' alt='parrot' height="200" width="200">

<br>
<br>
<h3></h3>

Sample Python application on Django with PostgreSQL database.

## Misson comlete!
### 1. Приложение докеризированно.
Не все возможности по уменьшению размера образа были использованы.
Можно отказаться от uWSGI в пользу gunicorn или попробовать
multistage сборку модулей Python с образом на базе Alpine.
### 2. Добавлена СУБД PostgreSQL.
Миграции прогоняются с помощью init-контейнера, хотя можно сделать и проще.
### 3. Добавлен Nginx в режиме reverse-proxy
Конфигурация Nginx минимальная, можно значительно улучшить.
Например вынести статику отдельно, связать Nginx с uWSGI через файловый сокет и т.д.
### 4. Все собрано вместе и даже работает :)
Добавлен healthcheck для приложения,
наружу выставлен только порт Nginx, остальное крутится внутри.
Также в коде были замечены метрики prometheus, но в задании
не сказано что с ними делать.

<h3>Requirements</h3>

____

- asgiref==3.7.2
- backports.zoneinfo==0.2.1
- Django==4.0.1
- django-prometheus==2.2.0
- Pillow==9.0.0
- prometheus-client==0.18.0
- psycopg2-binary==2.9.3
- sqlparse==0.4.4
- typing_extensions==4.8.0
- uWSGI==2.0.23

<h3>Deployment</h3>

____

- install Python 3.8
- install libs 
```shell
      pip3 install -r requirements.txt
```

* Set environment export for variables:
```yaml
      DJANGO_DB_HOST: db
      DJANGO_DB_NAME: app
      DJANGO_DB_USER: worker
      DJANGO_DB_PASS: worker
      DJANGO_DB_PORT: "5432"
      DJANGO_DEBUG: "False"
```

* migrate database:
```shell
python3 manage.py migrate
```
* start application:
```shell
python3 manage.py runserver 0.0.0.0:8000
```
