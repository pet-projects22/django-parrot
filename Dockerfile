FROM python:3.8-slim

WORKDIR /opt/app

RUN apt-get -y update &&\
    apt-get install -y gcc &&\
    rm -rf /var/lib/apt/lists/*
COPY ./requirements.txt .
RUN pip3 install --no-cache-dir --no-deps -r requirements.txt

COPY . .

EXPOSE 8000

CMD [ "uwsgi", "--http", ":8000", "--module", "main.wsgi" ]
